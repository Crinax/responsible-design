# Адаптивный дизайн

## Содержание

1. HTML
    1. Часто используемые несемантические теги
        - [div](https://developer.mozilla.org/ru/docs/Web/HTML/Element/div)
        - [p](https://developer.mozilla.org/ru/docs/Web/HTML/Element/p)
        - [span](https://developer.mozilla.org/ru/docs/Web/HTML/Element/span)
        - [h1, h2, h3](https://developer.mozilla.org/ru/docs/Web/HTML/Element/Heading_Elements)
        - [a](https://developer.mozilla.org/ru/docs/Web/HTML/Element/A)
        - [button](https://developer.mozilla.org/ru/docs/Web/HTML/Element/button)
        - [form](https://developer.mozilla.org/ru/docs/Web/HTML/Element/form)
        - [img](https://developer.mozilla.org/ru/docs/Web/HTML/Element/img)
        - [input](https://developer.mozilla.org/ru/docs/Web/HTML/Element/Input)
        - [table](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table), [tr](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr), [td](https://developer.mozilla.org/ru/docs/Web/HTML/Element/td)
2. CSS
    1. Часто используемые правила позиционирования:
        - [position](https://developer.mozilla.org/ru/docs/Web/CSS/position)
        - [padding](https://developer.mozilla.org/ru/docs/Web/CSS/padding)
        - [margin](https://developer.mozilla.org/ru/docs/Web/CSS/margin)
    2. Часто используемые правила размера:
        - [font-size](https://developer.mozilla.org/ru/docs/Web/CSS/font-size)
        - [width](https://developer.mozilla.org/ru/docs/Web/CSS/width)
        - [height](https://developer.mozilla.org/ru/docs/Web/CSS/height)
        - [max-width](https://developer.mozilla.org/ru/docs/Web/CSS/max-width) ([min-width](https://developer.mozilla.org/ru/docs/Web/CSS/min-width))
        - [max-height](https://developer.mozilla.org/ru/docs/Web/CSS/max-height) ([min-height](https://developer.mozilla.org/ru/docs/Web/CSS/min-height))
    3. Часто используемые единицы измерения:
        - [px](https://developer.mozilla.org/en-US/docs/Web/CSS/length#px)
        - [vh](https://developer.mozilla.org/en-US/docs/Web/CSS/length#vh) ([vw](https://developer.mozilla.org/en-US/docs/Web/CSS/length#vw))
        - [em](https://developer.mozilla.org/en-US/docs/Web/CSS/length#em) ([rem](https://developer.mozilla.org/en-US/docs/Web/CSS/length#rem))
        - [%](https://developer.mozilla.org/en-US/docs/Web/CSS/percentage)
    4. Виды отображения элементов ([display](https://developer.mozilla.org/ru/docs/Web/CSS/display))
        1. flexbox
            - [Дока](https://developer.mozilla.org/ru/docs/Web/CSS/flex)
            - [Игра c лягушками](http://flexboxfroggy.com/#ru)
            - [Игра Tower Defence](http://www.flexboxdefense.com/)
        2. grid
            - [Дока](https://developer.mozilla.org/ru/docs/Web/CSS/grid)
            - [Игра](http://cssgridgarden.com/#ru)

Так можешь поиграть в эту [игру](https://flukeout.github.io/) она больше нацелена на
практику css-селекторов
